# VERSION 0.1
# AUTHOR:         Tyler Parisi <tyler@possumlodge.me>
# DESCRIPTION:    Image with DokuWiki & lighttpd
# TO_BUILD:       docker build -t registry.gitlab.com/sparky8251/dokuwiki .
# TO_RUN:         docker run -d -p 80:80 --name dokuwiki registry.gitlab.com/sparky8251/dokuwiki

FROM ubuntu:18.04
MAINTAINER Tyler Parisi <tyler@possumlodge.me>

# Set the version you want of DokuWiki
# If not version 2017-02-19e, calculate the md5sum and set it in DOKUWIKI_CSUM
ENV DOKUWIKI_VERSION=2017-02-19e
ARG DOKUWIKI_CSUM=09bf175f28d6e7ff2c2e3be60be8c65f

# Update, install, & cleanup packages
RUN DEBIAN_FRONTEND=noninteractive \
    ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install \
        libterm-readline-gnu-perl \
        wget \
        curl \
        lighttpd \
        php-cgi \
        php-gd \
        php-ldap \
        php-curl \
        php-xml \
        php-mbstring && \
    apt-get clean autoclean && \
    apt-get autoremove && \
    rm -rf /var/lib/{apt,dpkg,cache,log}

# Download, check, deploy, & cleanup dokuwiki
RUN wget -q -O /dokuwiki.tgz "http://download.dokuwiki.org/src/dokuwiki/dokuwiki-$DOKUWIKI_VERSION.tgz" && \
    if [ "$DOKUWIKI_CSUM" != "$(md5sum /dokuwiki.tgz | awk '{print($1)}')" ];then echo "Wrong md5sum of downloaded file!"; exit 1; fi && \
    mkdir /dokuwiki && \
    tar -zxf dokuwiki.tgz -C /dokuwiki --strip-components 1

# Set up ownership
RUN chown -R www-data:www-data /dokuwiki

# Configure lighttpd
ADD ./conf/lighttpd/20-dokuwiki.conf /etc/lighttpd/conf-available/20-dokuwiki.conf
RUN lighty-enable-mod dokuwiki fastcgi accesslog
RUN mkdir /var/run/lighttpd && chown www-data.www-data /var/run/lighttpd

COPY docker-startup.sh /startup.sh
COPY tests/response.sh /response.sh

EXPOSE 80
VOLUME ["/dokuwiki/data/","/dokuwiki/lib/plugins/","/dokuwiki/conf/","/dokuwiki/lib/tpl/","/var/log/"]

ENTRYPOINT ["/startup.sh"]
CMD ["run"]
